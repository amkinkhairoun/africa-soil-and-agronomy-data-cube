---
title: "Africa Soil and Agronomy Data Cube (tutorial)"
author: "Created and maintained by: Tom Hengl (tom.hengl@envirometrix.net) | "
date: "Last compiled on: `r format(Sys.time(), '%d %B, %Y')`"
output: 
  github_document:
    toc: true
    toc_depth: 4
bibliography: ./tex/Hengl_et_al_2021_Nature_Sci_Rep.bib
csl: ./tex/apa.csl  
fig_caption: yes
link-citations: yes
twitter-handle: envirometri
header-includes:
- \usepackage{caption}
---

[<img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" />](http://creativecommons.org/licenses/by-sa/4.0/)

This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).

```{r, include=FALSE}
ls <- c("rgdal", "terra", "spatstat", "landmap")
new.packages <- ls[!(ls %in% installed.packages()[,"Package"])]
if(length(new.packages)) install.packages(new.packages)
#load("africa_data.RData")
library(captioner)
fig_nums <- captioner(prefix = "Fig.")
options(warn=-1)
```

## ![alt text](tex/R_logo.svg.png "About") Introduction

#### Purpose of this tutorial

This data processing tutorial explains how to: (1) access various land cover, soil, terrain and climatic layers for whole of Africa and at various resolutions (from 30-m to 1-km), (2) use the data to derive stocks, (3) aggregate per land cover class and/or admin units, and (4) generate sampling designs using different algorithms and aiming at improving accuracy of predictions (locally). A more general tutorial explaining how to access global layers (Cloud-Optimized GeoTIFFs) from www.OpenLandMap.org is [available here](https://gitlab.com/openlandmap/global-layers/-/blob/master/tutorial/OpenLandMap_COG_tutorial.md).

The main source of data used in this tutorial include:

- [**iSDAsoil**](https://isda-africa.com/isdasoil) layers representing soil properties and nutrients at two standard depth intervals 0–20 and 20–50 cm,
- **Sentinel-2 cloud-free mosaics** (prepared for the purpose of iSDAsoil project),
- **Digital Terrain Model (DTM)** based on ALOS AW3D30 and NASADEM and DTM derivatives (prepared for the purpose of iSDAsoil project),
- [**OpenLandMap layers**](https://openlandmap.org) (from 250-m to 1-km resolution),
- [Population map of Africa for 2018 at 30-m resolution](https://data.humdata.org/dataset/dbd7b22d-7426-4eb0-b3c4-faa29a87f44b) (Facebook Connectivity Lab and Center for International Earth Science Information Network — CIESIN — Columbia University. 2016. High Resolution Settlement Layer HRSL);

To cite iSDAsoil layers and the auxiliary data documented in this tutorial please use:

- Hengl, T., Miller, M.A.E., Križan, J. et al. (2021) **African soil properties and nutrients mapped at 30 m spatial resolution using two-scale ensemble machine learning**. Sci Rep 11, 6130. https://doi.org/10.1038/s41598-021-85639-y

Predictive modeling using 2--scale framework is also documented in detail [here](https://gitlab.com/openlandmap/spatial-predictions-using-eml). Production of the iSDAsoil dataset and app is discussed in detail in this [article](https://medium.com/isda-africa/soil-science-and-smallholders-the-contribution-of-soil-to-sustainable-african-agriculture-35a1f67c19a8).

If you prefer to use **python** for processing Cloud-Optimized GeoTIFFs, please refer to [this tutorial](https://gitlab.com/geoharmonizer_inea/eumap/-/blob/master/demo/python/05_cloud_optimized_geotiff.ipynb).

_Important note_: We do **NOT** recommend downloading whole GeoTIFFs of Africa at 30-m resolution as these are usually 10--20GB in size (per file). The total size of the repository at the moment exceeds 1.5TB. Instead, if you need to analyze whole land mask of Africa, we recommend downloading the files directly from [**zenodo.org**](https://zenodo.org/search?page=1&size=20&q=isdasoil). Also note that nutrient stocks and aggregated soil properties can be derived using variety of procedures (see e.g. @hengl2019predictive) and the total values might eventually differ.

If you discover an issue or a bug please report.

#### Listing all layers available

To list all layers available at 30-m resolution for whole of Africa please use [**this table**](./csv/wasabi-africa-soil.csv).
To list all layers available at 250-m resolution (global land mask) please use [**this table**](https://gitlab.com/openlandmap/global-layers/-/tree/master/tables/openlandmap_wasabi_files.csv).
Note: the file versions might change hence your code would need to be updated. Please subscribe to this repository or refer to https://isda-africa.com for the most up-to-date information about iSDAsoil.

The Sentinel mosaics for Africa (prepared by [MultiOne.hr](https://multione.hr)) are relatively large in size and might still contain artifacts between scenes and missing values beyond water bodies etc. The population density map at 30-m spatial resolution does **NOT** include some areas such as Sudan's and Somalia.

#### Opening data in QGIS 

Before you start running any type of analysis, we recommend that you first visualize the data using QGIS and familiarize yourself with it. Zoom in on the area of interest and see if you can observe any potential problems with that data such as:

- inconsistent land / water mask,
- incorrect missing value flag,
- measurement units not matching the description,
- spatial patterns not matching the description,

To add any of the layers listed above follow these steps in QGIS:

- Select _"Layer"_ --> _"Add Layer"_ --> _"Add Raster layer"_;
- Select _"Source Type"_ --> _"Protocol HTTP(s)/Cloud"_;
- Enter the URL of the layer and leave _"No authentication"_;

Example of a URL for the soil texture layer is:

```
https://s3.eu-central-1.wasabisys.com/africa-soil/layers30m/
    sol_texture.class_m_30m_20..50cm_2001..2017_africa_epsg4326_v0.1.tif
```

To attach some applicable QGIS legend, you can use the global soil data legends available via the [OpenLandMap.org](https://gitlab.com/openlandmap/global-layers/-/tree/master/SLD). This is an example of the layer visualized in QGIS:

<img src="img/preview_isdasoil_qgis.png" width="650" />  

_Figure: iSDAsoil layers (soil texture class) visualized in QGIS._

See also the [geo-harmonizer tutorial](https://opendatascience.eu/geo-harmonizer/geospatial-data-tutorial/) for more details about how to use COG files in QGIS.

To visualize Sentinel-2 cloud-free bands for Africa, you can use e.g.:

```
https://s3.eu-central-1.wasabisys.com/africa-soil/layers30m/
    lcv_b8a_sentinel.s2l2a_d_30m_s0..0cm_2018..2019.s22_africa_proj.laea_v0.1.tif
```

This is basically 25% probability quantile range for the Sentinel S2L2a band B8A computed from some 20TB of input data for the season 2 for years 2018 and 2019 using the Amazon AWS Sentinel-2 service. To learn more about how were the Sentinel-2 mosaics produced, please refer to @hengl2021african.

<img src="img/example_Sentinel_image_africa.jpg" width="650" />  

_Figure: Sentinel-2 mosaic for Africa visualized in QGIS (left), zoom-in on the farm plot in South Africa (right)._


## ![alt text](tex/R_logo.svg.png "Computing") Computing with COG in R

#### Crop maps and get values for polygons of interest

To crop maps of interest for the area of interest we recommend using the **rgdal** and **terra** packages [@hijmans2020package]. Consider, for example, a polygon map representing a 1-square-km farm in South Africa and covering diversity of agricultural fields:

```{r}
library(rgdal)
library(terra)
library(raster)
pol = readOGR("./data/farm_example.kmz")
pol.v <- vect(pol)
```

<img src="img/example_soilfarm_ge.jpg" width="750" />  

_Figure: Polygon for a farm digitized in Google Earth._

We can access and crop Potassium concentrations for the area of interest by using:

```{r}
tif.cog = paste0("/vsicurl/https://s3.eu-central-1.wasabisys.com/africa-soil/layers30m/", 
               c("sol_log.k_mehlich3_m_30m_0..20cm_2001..2017_africa_epsg4326_v0.1.tif",
               "sol_log.k_mehlich3_md_30m_0..20cm_2001..2017_africa_epsg4326_v0.1.tif",
               "sol_log.k_mehlich3_m_30m_20..50cm_2001..2017_africa_epsg4326_v0.1.tif",
               "sol_log.k_mehlich3_md_30m_20..50cm_2001..2017_africa_epsg4326_v0.1.tif"))
```

where `_m_30m_0..20cm` and `_m_30m_20..50cm` are the predicted extractable K in log-scale, and `_md_30m_0..20cm` and `_md_30m_20..50cm` are associated prediction errors (1-std).

We can load the values to R by using: 

```{r}
sol30m = lapply(tif.cog, function(i){crop(rast(i), pol.v)})  
sol30m.sp = do.call(cbind, lapply(sol30m, function(i){as.data.frame(i)}))
str(sol30m.sp)
```

Next, we need to back-transform values from log-scale to ppms: 

```{r}
sol30m.sp$k_mehlich3_l1 = expm1(sol30m.sp$sol_log.k_mehlich3_m_30m_0..20cm_2001..2017_africa_epsg4326_v0.1 / 10)
sol30m.sp$k_mehlich3_l2 = expm1(sol30m.sp$sol_log.k_mehlich3_m_30m_20..50cm_2001..2017_africa_epsg4326_v0.1 / 10)
```

Crop to the area of interest using the polygon map:

```{r}
pol.r = rasterize(pol.v, sol30m[[1]])
sol30m.m = as(as(raster(pol.r), "SpatialGridDataFrame"), "SpatialPixelsDataFrame")
sol30m.m$k_mehlich3_l1 = sol30m.sp$k_mehlich3_l1[sol30m.m@grid.index]
sol30m.m$k_mehlich3_l2 = sol30m.sp$k_mehlich3_l2[sol30m.m@grid.index]
```

We can finally plot extractable Potassium (K) values for the farm for two depth intervals (`l1` = 0–20-cm and `l2` = 20–50-cm) using:

```{r, map-log.k, fig.width=8, out.width="70%", fig.cap="Extractable Potassium in ppm for top and sub-soil."}
spplot(sol30m.m[c("k_mehlich3_l1","k_mehlich3_l2")])
```

According to the FAO guidelines [@roy2006plant] K concentration ranging from 0–50-ppm is very low (<50% expected yield), 50–100 is low, 100–175 is medium (80–100% yield) and >175 is high (100% yield).

#### Calculate stocks in t/ha

In the previous examples we have download and cropped images with predictions of Potassium (K) in ppm. We would next like to derive total stocks per farm. For this we need to also download bulk density and coarse fragments maps and import them into session:

```{r}
bld.cog = paste0("/vsicurl/https://s3.eu-central-1.wasabisys.com/africa-soil/layers30m/", 
               c("sol_db_od_m_30m_0..20cm_2001..2017_africa_epsg4326_v0.1.tif",
               "sol_log.wpg2_m_30m_0..20cm_2001..2017_africa_epsg4326_v0.1.tif"))
bld30m = lapply(bld.cog, function(i){crop(rast(i), pol.v)})  
bld30m.sp = do.call(cbind, lapply(bld30m, function(i){as.data.frame(i)}))
bld30m.sp$bld_l1 = bld30m.sp$sol_db_od_m_30m_0..20cm_2001..2017_africa_epsg4326_v0.1 * 10
bld30m.sp$wpg_l1 = expm1(bld30m.sp$sol_log.wpg2_m_30m_0..20cm_2001..2017_africa_epsg4326_v0.1 / 10)
```

Next, we convert the values from ppm to kg/m-square by using [@hengl2019predictive]:

```{r}
sol30m.m$k_mehlich3_l1s = sol30m.m$k_mehlich3_l1/1e6 * 
       bld30m.sp$bld_l1[sol30m.m@grid.index] * 
       (1 - bld30m.sp$wpg_l1[sol30m.m@grid.index]/100) * 0.2
summary(sol30m.m$k_mehlich3_l1s)
```

To derive total stocks in kg, we need to resample the summary map to equal area projection so we can estimate areas in square-m. One suitable projection system for this is the [Lambert Azimuthal Equal Area projection system](https://proj.org/operations/projections/laea.html):

```{r, map-stock, fig.width=8, out.width="70%", fig.cap="Potassium stock in kg/m-square for top-soil."}
prj.laea = "+proj=laea +lat_0=5 +lon_0=20 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs +ellps=WGS84 +towgs84=0,0,0"
pol.xy = project(pol.v, prj.laea)
r30m = rast(ext(pol.xy), crs=prj.laea, resolution=30)
sol30m.s = project(rast(raster(sol30m.m["k_mehlich3_l1s"])), r30m)
plot(sol30m.s)
```

So the total stock for the whole farm for 0–20-cm depth in tonnes is:

```{r}
sum(as.data.frame(sol30m.s)*30^2, na.rm=TRUE)/1e3
```


#### Aggregate values for different land cover classes 

To aggregate values for different land cover classes (see also [these worked out examples in R](https://soilmapper.org/SOC-chapter.html#predicting-ocs-from-point-data-the-2d-approach)) we have to also download the land cover map. We can use for this purpose the [Copernicus land cover map derived for year 2016 at 20-m spatial resolution](https://www.esa.int/ESA_Multimedia/Images/2017/10/African_land_cover) (important note: this product is discontinued and should be only used for testing purposes):

```{r}
lcv.cog = paste0("/vsicurl/https://s3.eu-central-1.wasabisys.com/africa-soil/layers30m/",
                 "lcv_land.cover_esa.cci.l4_m_30m_s0..0cm_2016_africa_proj.laea_v0.1.tif")
#GDALinfo(lcv.cog)
```

We can clip the same polygon using:

```{r}
lcv30m = crop(rast(lcv.cog), pol.xy)
```

We can check the nutrient stocks per land cover class by using:

```{r}
sol30m.sxy = as(as(raster(sol30m.s), "SpatialGridDataFrame"), "SpatialPixelsDataFrame")
sol30m.sxy$lcv = as.factor(as.data.frame(lcv30m)[sol30m.sxy@grid.index,1])
library(plyr)
sol_agg.lu <- plyr::ddply(sol30m.sxy@data, .(lcv), summarize,
                          Total_K_kg=sum(k_mehlich3_l1s*30^2, na.rm=TRUE),
                          Area_m2=sum(!is.na(k_mehlich3_l1s))*30^2)
sol_agg.lu$Total_K_M = sol_agg.lu$Total_K/(sol_agg.lu$Area_m2)
sol_agg.lu[,c("lcv", "Total_K_kg", "Area_m2", "Total_K_M")]
```

which shows that the [class](./csv/ESACCI-LC_S2_Prototype_ColorLegend.csv) `3` (Grassland) has the relatively highest K concentration.

#### Generate sampling plan using the mapping uncertainty

One efficient way to generate sampling designs to improve maps of Potassium in soil is to use the existing error map, then sample proportionally to the prediction variance, collect new points and re-build prediction models until some required accuracy level is reached [@hengl2018random]. This is referred to as _"uncertainty-guided sampling"_ by @stumpf2017uncertainty and is best illustrated by the figure below.

<img src="img/1-s2.0-S0341816217300401-gr2.jpg" width="500" />  

_Figure: Uncertainty-guided sampling based on [Stumpf et al. (2016)](https://dx.doi.org/10.1016/j.catena.2017.01.033). The first sample is used to build initial model, then the 2nd-round sampling focuses on area of highest uncertainty and is used to rebuild the model._

The uncertainty for mapping K for the farm above is (for top-soil):

```{r}
unc.cog = paste0("/vsicurl/https://s3.eu-central-1.wasabisys.com/africa-soil/layers30m/", 
               "sol_log.k_mehlich3_md_30m_0..20cm_2001..2017_africa_epsg4326_v0.1.tif")
unc30m = crop(rast(unc.cog), pol.v)
unc30m.xy = project(unc30m, r30m, method="bilinear")
pol.r.xy = rasterize(pol.xy, unc30m.xy)
unc30m.m = as(raster(unc30m.xy), "SpatialGridDataFrame")
unc30m.sp = as.data.frame(pol.r.xy)
unc30m.m$var = ifelse(is.na(unc30m.sp$X), NA,
                  unc30m.m$sol_log.k_mehlich3_md_30m_0..20cm_2001..2017_africa_epsg4326_v0.1^2)
unc30m.m = as(unc30m.m["var"], "SpatialPixelsDataFrame")
```

We can use the [spatstat](https://spatstat.org/) package to sample proportionally to prediction variance (i.e. use the prediction variance as weight map):

```{r, map-unc, fig.width=8, out.width="70%", fig.cap="Extractable Potassium in ppm for top and sub-soil."}
library(spatstat)
dens.var <- as.im(as.image.SpatialGridDataFrame(unc30m.m["var"]))
extra.pnts <- rpoint(40, f=dens.var)
plot(raster(unc30m.m))
points(extra.pnts$x, extra.pnts$y)
```

In this case the prediction errors (variance) is higher towards the western side of farm, and consequently more point samples are put in that part. Alternatively, one could also setup a threshold prediction error, then ONLY sample in the areas where the error is above certain value by using Latin Hypercube Sampling or similar (method explained in @stumpf2017uncertainty).


## ![alt text](tex/R_logo.svg.png "About") Other data and analysis possibilities

#### Other analysis possibilities

Other analysis possibilities with the iSDAsoil and other 30-m resolution layers include:

- Overlay yield data (farm plots or individual 30✕30-m pixels) and fit a regression model / try to model yields using soil terrain and climate parameters [@hengl2017soil],
- Derive soil organic carbon stock in the soil in kg/m-square and associated (propagated) uncertainty,
- Use the land cover and population density maps at county level and derive soil nutrient deficiencies and how they relate to different land cover classes and population densities,
- Aggregate values spatially using `gdalwarp` to e.g. 1-km spatial resolution and observe patterns per country,

#### Other data sources for Africa

Other data sources (not included in this Data Cube) and data portals for Africa with Earth Observation and similar data sets:

- **Planet NICFI** (https://www.planet.com/nicfi/): you can download the 5-meter resolution ARD imagery (sub-Sahara Africa only and CC-NC-Alike license only; project financed by the [Norwegian Government](https://www.nicfi.no/current/new-satellite-images-to-allow-anyone-anywhere-to-monitor-tropical-deforestation/));
- **Digital Earth Africa** (https://www.digitalearthafrica.org/): provides access to a map viewer and a sandbox to derive various products per farm / polygon (project funded by US-based Leona M. and Harry B. Helmsley Charitable Trust and the Australian Government);
- Africa Regional Data Cube **ARDC** (https://www.data4sdgs.org/index.php/initiatives/africa-regional-data-cube): provides various EO-based data products for Ghana, Kenya, Sierra Leone, Senegal, and Tanzania;
- **AfriAlliance** Africa-EU innovation alliance (https://afrialliance.org/): aims at providing climatic/meteorological and hydrological information;

<img src="img/preview_planet_5m_resolution_images.jpg" width="750" />  

_Figure: 5-m spatial resolution imagery is available publicly via the Planet Explorer._

Have in mind that the 3rd party data might be: (1) of different spatial resolution, (2) covering different time-periods (not overlapping in time), (3) of different accuracy including location accuracy. Consequently they might be incompatible with the Cloud-Optimized GeoTIFFs listed in this tutorial.

#### Acknowledgments

[Innovative Solutions for Decision Agriculture Ltd (iSDA)](https://isda-africa.com) is a social enterprise with the mission to improve smallholder farmer profitability across Africa. iSDA builds on the legacy of the African Soils information service (AfSIS) project. We are grateful for the outputs generated by all former AfSIS project partners: Columbia University, Rothamsted Research, World Agroforestry (ICRAF), Quantitative Engineering Design (QED), ISRIC — World Soil Information, International Institute of Tropical Agriculture (IITA), Ethiopia Soil Information Service (EthioSIS), Ghana Soil Information Service (GhaSIS), Nigeria Soil Information Service (NiSIS) and Tanzania Soil Information Service (TanSIS). More details on AfSIS partners and data contributors can be found at https://isda-africa.com/isdasoil 

#### Help improve the Africa soil property and nutrient maps!

If you are a soil data producer or are aware of soil profile, soil sample data described in literature or reports, please contact us so that we can add those points in the next major update of maps. We would attribute your contribution and provide support with translating the point data to analysis-ready data. Please contribute your point data and help make better predictions of soil / land resources in Africa.

- Contact for the iSDAsoil: https://www.isda-africa.com/isdasoil/faqs/; 

Once your point data is added to the bulk of the training data, we will use it to improve predictions and then notice you of the improvements.

```{r, include=FALSE}
save.image("africa_data.RData")
```

## References
